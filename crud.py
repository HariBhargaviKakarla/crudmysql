import mysql.connector
class crudoperations:

	def create_database():
		connection = mysql.connector.connect(
		host="localhost",
		database="hari",
  	user="root",
  	password="umaharivinay"
		)
		mycursor = connection.cursor()
		mycursor.execute("CREATE DATABASE hari")
		print("the database is created")

	def create_table():
		connection = mysql.connector.connect(
		host="localhost",
		database="hari",
  	user="root",
  	password="umaharivinay"
		)
		mycursor = connection.cursor()
		mycursor.execute("create table student(ename varchar(255),stuid varchar(255))")
		print("the table is created")

	def create_record():
		connection = mysql.connector.connect(
		host="localhost",
		database="hari",
  	user="root",
  	password="umaharivinay"
		)
		mycursor = connection.cursor()
		insert_command = "INSERT INTO student (ename, stuid) VALUES (%s, %s)"
		values = [
			("jishnu", '30504'),
			("laddu",'30492'),
			("karthik",'30450')
		]

		mycursor.executemany(insert_command, values)

		connection.commit()

		print(mycursor.rowcount, "are inserted.")
		print("the records are created")

	def read_database():
		connection = mysql.connector.connect(
		host="localhost",
		database="hari",
  	user="root",
  	password="umaharivinay"
		)
		mycursor = connection.cursor()
		mycursor.execute("SHOW DATABASES")
		for i in mycursor:
			print(i)
	def display_tables():
		connection = mysql.connector.connect(
		host="localhost",
		database="hari",
  	user="root",
  	password="umaharivinay"
		)
		mycursor = connection.cursor()
		mycursor.execute("SHOW tables")
		for i in mycursor:
			print(i)
	def read_table():
		connection = mysql.connector.connect(
		host="localhost",
		database="hari",
  	user="root",
  	password="umaharivinay"
		)
		mycursor = connection.cursor()
		mycursor.execute("select * from student")
		for i in mycursor:
			print(i)
	def rename_table():
		connection = mysql.connector.connect(
		host="localhost",
		database="hari",
  	user="root",
  	password="umaharivinay"
		)
		mycursor = connection.cursor()
		mycursor.execute("alter table student rename to student1")
		print("the name of the table is changed")
	def add_column_table():
		connection = mysql.connector.connect(
	host="localhost",
		database="hari",
  	user="root",
  	password="umaharivinay"
		)
		mycursor = connection.cursor()
		mycursor.execute("alter table student1 add email varchar(255)")
		print("the new column is added to the table")
	def drop_column_table():
		connection = mysql.connector.connect(
		host="localhost",
		database="hari",
  	user="root",
  	password="umaharivinay"
		)
		mycursor = connection.cursor()
		mycursor.execute("alter table student1 drop column email")
		print("the column of the table is deleted")
	def update_record():
		connection = mysql.connector.connect(
		host="localhost",
		database="hari",
  	user="root",
  	password="umaharivinay"
		)
		mycursor = connection.cursor()
		mycursor.execute("update student set name="raj",stuid='30459' where name="raju"")
		print("the record is updated")
	def delete_record():
		connection = mysql.connector.connect(
		host="localhost",
		database="hari",
  	user="root",
  	password="umaharivinay"
		)
		mycursor = connection.cursor()
		mycursor.execute("delete from student1 where name="laddu" and stuid='30492'")
		print("the record is deleted ")
	def delete_table():
		connection = mysql.connector.connect(
		host="localhost",
		database="hari",
  	user="root",
  	password="umaharivinay"
		)
		mycursor = connection.cursor()
		mycursor.execute("drop table student")
		print("the table is deleted")


	def delete_database():
		connection = mysql.connector.connect(
		host="localhost",
		database="hari",
  	user="root",
  	password="umaharivinay"
		)
		mycursor = connection.cursor()
		mycursor.execute("delete database hari")
		print("the database is deleted")