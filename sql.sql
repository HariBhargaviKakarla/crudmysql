-- creating database
create database hari;
-- using created database
use hari;
-- creating table
create table vinay(
empid int,
name varchar(225));
-- inserting the values into table
insert into vinay(empid,name)
values(10989,"bhargavi"); 
insert into vinay(empid,name)
values(10990,"priya");
-- describing the context using in table
describe vinay;
-- display the database created
show databases;
select *from  vinay;
select empid, name FROM vinay;
select name from vinay;
select empid from vinay;
-- display the table created
show tables;
-- updating the table
update vinay
set empid = 10999 , name ="uma"
where empid = 10989; 
ALTER TABLE vinay
  RENAME TO sandeep;
  ALTER TABLE sandeep
ADD Email varchar(255);
ALTER TABLE sandeep
DROP COLUMN Email;
ALTER TABLE sandeep
MODIFY COLUMN empid varchar(255);
-- drop database hari
-- drop table sandeep

